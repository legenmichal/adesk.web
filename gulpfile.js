var gulp = require('gulp');
var uglify = require('gulp-uglify');
var minifyCSS = require('gulp-minify-css');
var sass = require('gulp-sass');
var templateBuilder = require('gulp-angular-templatecache');
var concat = require('gulp-concat');
var browserSync = require('browser-sync');
var del = require('del');
var addStream = require('add-stream');
var runSequence = require('run-sequence');

gulp.task('browserSync', function() {
  browserSync({
    server: {
      baseDir: 'src'
    },
  })
})

gulp.task('concat-js', function() {
  return gulp.src('./src/scripts/**/*.js')
    .pipe(addStream.obj(prepareTemplates()))
    .pipe(concat('main.js'))
    .pipe(gulp.dest('./src/js/'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

gulp.task('sass', function () {
  gulp.src('./src/sass/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('./src/css/'))    
    .pipe(browserSync.reload({
      stream: true
    }));
});

gulp.task('minify-css', function() {
  var opts = {comments:true, spare:true};
  gulp.src('./src/sass/*.scss')
    .pipe(sass())
    .pipe(minifyCSS(opts))
    .pipe(gulp.dest('./build/css/'))
});

gulp.task('minify-js', function() {
  gulp.src('./src/scripts/**/*.js')
    .pipe(addStream.obj(prepareTemplates()))
    .pipe(concat('main.js')) 
    .pipe(uglify())
    .pipe(gulp.dest('./build/js/'))
});

gulp.task('copy-bower-components', function () {
  gulp.src('./src/components/**')
    .pipe(gulp.dest('build/components/'));
});

gulp.task('copy-html-files', function () {
  gulp.src('./src/*.html')
    .pipe(gulp.dest('build/'));
});

gulp.task('copy-images', function () {
  gulp.src('./src/images/**')
    .pipe(gulp.dest('build/images/'));
});

gulp.task('clean-build', function () {
  del('build');
});

// http://paulsalaets.com/pre-caching-angular-templates-with-gulp/
function prepareTemplates() {
  return gulp.src('src/templates/**/*.html')
    .pipe(templateBuilder());
}

// default task
gulp.task('default', ['browserSync', 'concat-js', 'sass'], function(){
    gulp.watch('src/scripts/**/*.js', ['concat-js']);
    gulp.watch('src/**/*.html', ['concat-js']);
    gulp.watch('src/sass/*.scss', ['sass']);
});

gulp.task('build', function(callback) {
    runSequence('clean-build', ['sass', 'minify-js', 'copy-bower-components', 'copy-html-files', 'copy-images'], 'minify-css', callback);
});
