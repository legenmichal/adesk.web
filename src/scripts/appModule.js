angular.module('templates', []);

var appModule = angular.module('AppModule', ['templates', 'ui.bootstrap', 'ngRoute', 'app-services', 'app-constants', 'app-directives', 'ui.calendar' ]);

appModule.config(['$routeProvider', '$locationProvider', 'ACCESS_LEVELS', function($routeProvider, $locationProvider, ACCESS_LEVELS){
    //$locationProvider.html5Mode(true);
    
    $routeProvider
        .when('/', {
            templateUrl: 'pages/dashboard.html',
            controller: 'DashboardController',
            controllerAs: 'ctrl',
            accessLevel: ACCESS_LEVELS.admin,
            pageTitle: 'Dashboard'
        })
        .when('/users', {
            templateUrl: 'pages/users.html',
            controller: 'UsersController',
            controllerAs: 'ctrl',
            accessLevel: ACCESS_LEVELS.admin,
            pageTitle: 'Užívateľia'
        })
        .when('/users/new', {
            templateUrl: 'pages/newuser.html',
            controller: 'NewUserController',
            controllerAs: 'ctrl',
            accessLevel: ACCESS_LEVELS.admin,
            pageTitle: 'Nový užívateľ'
        })
        .when('/users/edit/:userId', {
            templateUrl: 'pages/edituser.html',
            controller: 'EditUserController',
            controllerAs: 'ctrl',
            accessLevel: ACCESS_LEVELS.admin,
            pageTitle: 'Detail užívateľa'
        })
        .when('/orders', {
            templateUrl: 'pages/orders.html',
            controller: 'OrdersController',
            controllerAs: 'ctrl',
            accessLevel: ACCESS_LEVELS.all,
            pageTitle: 'Objednávky'
        });
}]);

appModule.config(['$httpProvider', '$routeProvider', function($httpProvider, $routeProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];    
}]);

appModule.factory('appInterceptor', ['$rootScope', 'currentUser', 'systemPopupService', '$window', 'URLS', function($rootScope, currentUser, systemPopupService, $window, URLS) {
    var interceptor = {
        'responseError': function(rejection) {
            switch(rejection.status) {
                case 401:
                    currentUser.remove();
                    $window.location.href = URLS.Pages.Login;
                    break;
                case 404:
                    $location.path(URLS.Pages.Default);
                    break;
                case 500:
                case 403:
                    systemPopupService.showError("V aplikacii nastala chyba");
                    break;
            }
        },
        'request': function (config) {
            if(currentUser.isLoggedIn()){
                config.headers = config.headers || {};
                config.headers.Authorization = 'Bearer ' + currentUser.getToken();
            }

            return config;
        }
    }
    
    return interceptor;
}]);

appModule.config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('appInterceptor');
}]);

appModule.config(['uibDatepickerConfig', 'uibDatepickerPopupConfig', function(uibDatepickerConfig, uibDatepickerPopupConfig) {
    uibDatepickerConfig.startingDay = 1;
    uibDatepickerPopupConfig.datepickerPopup = "dd.MM.yyyy"; 
}]);

appModule.run(['$rootScope', '$location', '$window', 'currentUser', 'URLS', function($rootScope, $location, $window, currentUser, URLS) {
    $rootScope.$on('$routeChangeSuccess', function(evt, current) {
        $rootScope.pageTitle = current.pageTitle;
    });
                   
    $rootScope.$on('$routeChangeStart', function(evt, next, curr) {
        if (!currentUser.isLoggedIn()) {
            evt.preventDefault();
            $window.location.href = URLS.Pages.Login;
        }
        else if(!currentUser.isAuthorized(next.accessLevel)){
            $location.path(URLS.Pages.Default);
        }
    });
    
    $rootScope.logout = function() {
        currentUser.remove();
        $window.location.href = URLS.Pages.Login;
    };
}]);

