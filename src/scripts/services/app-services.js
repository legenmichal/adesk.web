var servicesModule = angular.module('app-services', ['ngStorage', 'app-constants']);

servicesModule.factory('loaderService', function($rootScope) {
    return {
        show: function(scope, callback) {
            $rootScope.$emit('show-loader');
        },

        hide: function() {
            $rootScope.$emit('hide-loader');
        }
    };
});

servicesModule.factory('securityService', ['$http', '$q', 'currentUser', function($http, $q, currentUser) {
    return {
        authenticate: function(username, password){
            var data = {
                Password: password,
                UserName: username
            }; 

            return $http.post("http://localhost/account/login/", data).then(function(data) {
                currentUser.set(data.data);
            });
        }
    }
}]);

servicesModule.factory("currentUser", ['$sessionStorage', "ACCESS_LEVELS", function($sessionStorage, ACCESS_LEVELS){
    var _user = $sessionStorage.user;

    var set = function(user) {
        if (user.IsAdmin) {
            user.role = ACCESS_LEVELS.admin;
        }
        else {
            user.role = ACCESS_LEVELS.user;
        }
        _user = user;

        $sessionStorage.user = _user;
    }
    
    return {
        isAuthorized: function(lvl) {
            return _user.role >= lvl;
        }, 
        isLoggedIn: function() {
            return _user ? true : false;
        },  
        remove: function(){
            _user = null;
            $sessionStorage.$reset();
        },
        set: set,
        getUser: function() {
            return _user;
        },
        getId: function() {
            return _user ? _user._id : null;
        },
        getToken: function() {
            return _user ? _user.Token : '';
        }
    }
}]);

servicesModule.factory("notificationService", [function(){
    var callbacks = [];
    return {
        register: function(cb){
            callbacks.push(cb);
        },
        notify: function(message){
            angular.forEach(callbacks, function(callback){
                callback(message);
            });
        }
    };
}]);

servicesModule.factory("systemPopupService", ['$q', 'POPUP_TYPE', function($q, POPUP_TYPE){
    var notifyEvent;
    var context = {
        promise: null,
        type: null,
        message: null
    };
    
    var show = function(message, type){
        context.type = type;
        context.message = message;
        context.promise = $q.defer();
        notifyEvent();
        return context.promise.promise;
    }
    
    return {
        register: function(cb){
            notifyEvent = cb;
        },
        showError: function(message){
            return show(message, POPUP_TYPE.error);
        },
        showConfirmation: function(message){
            return show(message, POPUP_TYPE.confirmation);
        },
        showWarning: function(message){
            return show(message, POPUP_TYPE.warning);
        },
        context: context
    };
}]);

servicesModule.factory("usersService", ['$q', '$http', 'URLS', function($q, $http, URLS){
    return {
        getList: function(take, skip, includeTotalCount, filter){
            includeTotalCount = includeTotalCount || false;
            
            var params = {
                take: take, 
                skip: skip,
                includeTotalCount: includeTotalCount
            }
            angular.extend(params, filter);
            
            return $http.get(URLS.Api.Users.List, { params: params });
       },
        
       search: function(expression){
           var filter = {
               expression: expression
           }
           return $http.get(URLS.Api.Users.Search, { params: filter } );
       },
        
       save: function(entity){
            return $http.post(URLS.Api.Users.Save, entity);        
       },
        
       get: function(id){
            var params = {
                id: id
            }
            
            return $http.get(URLS.Api.Users.Get, { params: params });        
       },
        
       remove: function(id){
            var params = {
                id: id
            }
            
            return $http.get(URLS.Api.Users.Remove, { params: params }); 
       }
    };
}]);

servicesModule.factory("ordersService", ['$q', '$http', 'URLS', function($q, $http, URLS){
    return {
        getListByDate: function(date){
            var params = {
            }
            angular.extend(params, {date: date});
            
            return $http.get(URLS.Api.Orders.GetList, { params: params });
       },
        
       save: function(entity){
            return $http.post(URLS.Api.Orders.Save, entity);        
       },
        /*
       get: function(id){
            var params = {
                id: id
            }
            
            return $http.get(URLS.Api.Users.Get, { params: params });        
       },
        
       remove: function(id){
            var params = {
                id: id
            }
            
            return $http.get(URLS.Api.Users.Remove, { params: params }); 
       }
       */
    };
}]);

servicesModule.factory("orderTypesService", ['$q', '$http', 'URLS', function($q, $http, URLS){
    return {
        getList: function(filter){
            return $http.get(URLS.Api.OrderTypes.List);
       },
    };
}]);