var directivesModule = angular.module('app-directives', ['ngStorage', 'app-services']);

directivesModule.directive("appNav", ["currentUser", "$templateCache", "$compile", function(currentUser, $templateCache, $compile){
    return {
        restrict: 'E',
        scope: {},
        replace: true,
        link: function(scope, element, attrs){
            var template = "";
            
            if (currentUser.isLoggedIn()) {
                if (currentUser.getUser().IsAdmin) {
                    template = $templateCache.get('adminNavigation.html');
                }
                else {
                    template = $templateCache.get('userNavigation.html');
                }
            }
            
            element.html(template);
            $compile(element.contents())(scope);
        }
    };
}]);

directivesModule.directive("notification", ["notificationService", function(notificationService){
    return {
        restrict: 'E',
        scope: {},
        replace: true,
        template: 
            '<div class="notification"></div>',
        link: function(scope, element, attrs){
            var showNotification = function(message){
                element.text(message);
                element.addClass('active');
                setTimeout(function() {
                    element.removeClass('active');
                }, 2000);
            }
            
            notificationService.register(showNotification);
        }
    }
}]);

directivesModule.directive("systemPopup", ["systemPopupService", "POPUP_TYPE", function(systemPopupService, POPUP_TYPE){
    return {
        restrict: 'E',
        scope: {},
        replace: true,
        template: 
            '<div ng-show="showConfirmation" class="system-popup">' +
                '<header>' +
                    '{{header}}'+
                '</header>' +
                '<main>' +
                    '<span> ' +
                        '{{message}}' +
                    '</span>' +
                '</main>' +
                '<div class="operations">' +
                    '<button ng-click="confirm()">Ok</button>' +
                    '<button ng-show="isConfirmation" ng-click="cancel()">Zrušiť</button>' +
                '</div>' +
            '</div>',
        link: function(scope, element, attrs){
            var showPopup = function(message, type){                
                switch(systemPopupService.context.type){
                    case POPUP_TYPE.error:
                        scope.header = "Chyba";
                        break;
                    case POPUP_TYPE.confirmation:
                        scope.header = "Potvrdenie";
                        break;
                    case POPUP_TYPE.warning:
                        scope.header = "Upozornenie";
                        break;
                }
                
                scope.isConfirmation = systemPopupService.context.type == POPUP_TYPE.confirmation;
                scope.message = systemPopupService.context.message;
                
                scope.showConfirmation = true;
            }
            
            systemPopupService.register(showPopup);

            
            scope.confirm = function(){
                systemPopupService.context.promise.resolve(); 
                scope.showConfirmation = false;
            }
            
            scope.cancel = function(){
                systemPopupService.context.promise.reject();    
                scope.showConfirmation = false;
            }
        }
    }
}]);

directivesModule.directive('loader', ['$timeout', '$rootScope', function($timeout, $rootScope){
    return {
        restrict: 'E',
        scope: {
            show: '=',
            delay: '@'
        },
        replace: true,
        template: 
        '<div class="loader hidden">' + 
            '<div class="loader-wrapper">' + 
                '<div class="spinner"></div>' + 
            '</div>' + 
        '</div>',
        link: function(scope, element, attrs){
            var showTimer;
            
            $rootScope.$on('show-loader', showSpinner);
            
            $rootScope.$on('hide-loader', hideSpinner);
            
            scope.$watch('show', function(newVal, oldVal){
                if(newVal === oldVal){
                    return;
                }
                newVal ? showSpinner() : hideSpinner();
            });

            function showSpinner() {
              if (showTimer){ 
                  return;
              }

              showTimer = $timeout(showElement.bind(this, true), getDelay());
            }

            function hideSpinner() {
              if (showTimer) {
                $timeout.cancel(showTimer);
              }

              showTimer = null;
              showElement(false);
            }

            function showElement(show) {
              show ? element.removeClass('hidden') : element.addClass('hidden');
            }

            function getDelay() {
              return angular.isDefined(scope.delay) ? parseInt(scope.delay) : 200;
            }
        }
    }
}]);

directivesModule.directive('navigationLink', ['$rootScope', function($rootScope){
    return {
        restrict: 'A',
        scope: {},
        link: function (scope, element, attrs){
            $rootScope.$on('$routeChangeSuccess', function(evt, route) {
                if (route.originalPath == attrs.navigationLink){
                    element.addClass('selected');
                }
                else {
                    element.removeClass('selected');
                }
            });
        }
    }
}]);

directivesModule.directive("appCloak", ["currentUser", function(currentUser){
    return {
        restrict: 'A',
        compile: function(element, attr) {
            attr.$set('appCloak', undefined);
            if (!currentUser.isLoggedIn()) {
                element.addClass('hidden');
            }
        }
    }
}]);

directivesModule.directive("appGrid", ['$rootScope', '$sessionStorage', 'loaderService', function($rootScope, $sessionStorage, loaderService){
    var pageSize = 10;
    
    return {
        restrict: 'E',
        scope: {
            api: '=',
            filter: '='
        },
        templateUrl: function(tElement, tAttrs) {
            return tAttrs.templateurl;
        },
        link: function (scope, element, attrs){
            scope.currentPage = 1;
            
            var load = function(includeTotalCount){
                includeTotalCount = includeTotalCount || false;
                var skip = (scope.currentPage - 1) * pageSize;
                
                loaderService.show();
                scope.api.load(pageSize, skip, includeTotalCount, scope.filter).then(function(data){
                    loaderService.hide();
                    if (includeTotalCount){
                        scope.maxPage = Math.floor((data.data.TotalCount / pageSize)) + ((data.data.TotalCount % pageSize) > 0 ? 1 : 0);
                    }
                    scope.data = data.data.Data;
                    
                    // Save current state to cache
                    saveState();
                });
            }
            
            var saveState = function(){
                if (!angular.isDefined(attrs.cache)) {
                    return;
                }
                
                var state = {
                    filter: scope.filter,
                    currentPage: scope.currentPage,
                    maxPage: scope.maxPage,
                    data: scope.data 
                }

                $sessionStorage[attrs.cache] = state;
            }
            
            var loadState = function(){
                if (!angular.isDefined(attrs.cache)) {
                    return false;
                }
                
                // Restore state
                var state = $sessionStorage[attrs.cache];
                
                if (angular.isDefined(state)){
                    scope.filter = state.filter;
                    scope.currentPage = state.currentPage;
                    scope.maxPage = state.maxPage;
                    
                    return true;
                }
                
                return false;
            }
            
            scope.goToDetail = function(id){
                scope.api.goToDetail(id);
            }
            
            scope.goToNextPage = function(){
                if (scope.currentPage < scope.maxPage){
                    scope.currentPage++;
                    load();
                }
            }
            
            scope.goToPreviousPage = function(){
                if (scope.currentPage > 1){
                    scope.currentPage--;
                    load();
                }
            }
            
            scope.$watch('filter', function(newValue, oldValue){
                if(newValue === oldValue){
                    return;
                }
                
                scope.currentPage = 1;
                load(true);
            }, true);
            
            if(!loadState()){
                load(true);
            } 
            else {
                load(false);
            }
        }
    }
}]);

directivesModule.directive("appForm", [function(){
    var form;
    
    return {
        restrict: 'E',
        scope: {
            api: '=',
            formObject: '='
        },
        templateUrl: function(tElement, tAttrs) {
            return tAttrs.templateurl;
        },
        link: function (scope, element, attrs){
            form = element.find("#form");
            
            scope.api.isValid = function(){
                if (!scope.form.$valid) {
                    form.addClass("submitted");
                }
                
                return scope.form.$valid;
            }
        }
    }
}]);

directivesModule.directive("orderWindow", ['orderTypesService', function(orderTypesService){
    return {
        restrict: 'E',
        scope: {
            api: '='
        },
        templateUrl: function(tElement, tAttrs) {
            return 'forms/orderForm.html';
        },
        replace: true,
        link: function (scope, element, attrs){
            var form = element.find("#form");
            
            scope.api.show = function(order){
                loadTypes();
                scope.order = order;
                scope.showWindow = true;
            }
            
            var loadTypes = function(){
                orderTypesService.getList().then(function(data){
                    scope.orderTypes = data.data;
                });            
            } 
            
            scope.save = function(){
                if (!scope.form.$valid) {
                    form.addClass("submitted");
                }
                else {
                    scope.api.save(scope.order);
                    scope.showWindow = false;
                }
            }
            
            scope.close = function(){
                scope.showWindow = false;
            }
        }
    }
}]);

directivesModule.directive("autocomplete", [function(){
    return {
        restrict: 'E',
        transclude: true,
        template:
        '<div class="form-group" ng-transclude>'+
        '</div>',
        replace: true,
        controller: ['$scope', function($scope) {
            var optionsScope, inputScope; 
            
            this.select = function(selectedValue){
                inputScope.select(selectedValue);
            }
            
            this.showAutocomplete = function(data){
                optionsScope.showOptions(data);
            }
            
            this.registerOptionsControl = function(scope){
                optionsScope = scope;
            }
            
            this.registerInputControl = function(scope){
                inputScope = scope;
            }
            
            this.hideAutocomplete = function(){
                optionsScope.hideOptions();
            }
        }]
    }
}]);

directivesModule.directive("autocompleteOptions", [function(){
    return {
        restrict: 'E',
        scope: {},
        require: ['^autocomplete'],
        template:
            '<ul ng-show="isVisible" class="autocomplete-options" >'+
                '<li ng-repeat="option in options" ng-click="select(option)" >{{::option.Name}}</li>'+
            '</ul>',
        replace: true,
        link: function (scope, element, attrs, ctrls){
            var acCtrl = ctrls[0];
                                    
            acCtrl.registerOptionsControl(scope);
            
            scope.select = function(selectedOption){
                scope.isVisible = false;
                acCtrl.select(selectedOption);                    
            }
            
            scope.showOptions = function(data){
                scope.isVisible = true;
                scope.options = data;
            }
            
            scope.hideOptions = function(){
                scope.isVisible = false;
            }
        }
    }
}]);

directivesModule.directive("autocompleteInput", ['usersService', function(usersService){
    return {
        restrict: 'A',
        scope: {
            order: '=autocompleteInput'
        },
        require: ['^autocomplete', 'ngModel'],
        link: function (scope, element, attrs, ctrls){
            var acCtrl = ctrls[0];
            var ngModelCtrl = ctrls[1];
            var focused = false;
            
            acCtrl.registerInputControl(scope);
            
            element.on('focus', function(){ focused = true; })
            element.on('blur', function(){ focused = false; })
            element.on('keydown', function(event){ 
                scope.$apply(function(){
                    if (event.keyCode == 40){ // Down
                        load(ngModelCtrl.$viewValue);
                    } 
                    else if(event.keyCode == 27){ // Esc
                        acCtrl.hideAutocomplete();
                    }
                    else if(event.keyCode != 37 || event.keyCode != 38 || event.keyCode != 39){
                        ngModelCtrl.$setValidity('invalid', false);
                    }
                });
            })
            
            scope.$watch(attrs.ngModel, function(value) {
                if (focused){
                    if (value && value.length > 4){
                        load(value);
                    }
                    else {
                        acCtrl.hideAutocomplete();
                    }
                }
            });
            
            // todo - vysunut load a select do controlleru
            var load = function(value){
                usersService.search(value).then(function(data){
                    acCtrl.showAutocomplete(data.data);
                });
            } 
            
            scope.select = function(selectedValue){
                scope.order.UserName = selectedValue.Name;
                scope.order.UserId = selectedValue.Id;
                
                ngModelCtrl.$setValidity('invalid', true);
            }
        }
    }
}]);