var loginModule = angular.module('LoginModule', ['app-services', 'app-constants', 'app-directives']);

loginModule.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
}]);


