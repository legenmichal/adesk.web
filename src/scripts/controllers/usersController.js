var appModule = angular.module('AppModule');

appModule.controller('UsersController',['usersService', '$location', 'URLS', function(usersService, $location, URLS){
    this.filter = {
    }
    
    this.api = {
        load: usersService.getList,
        goToDetail: function(userId){
            $location.path(URLS.Pages.Users.Edit.replace('{0}', userId));
        }
    }
    
    this.create = function(){
        $location.path(URLS.Pages.Users.New);
    }
}]);

appModule.controller('NewUserController',['$location', 'usersService', 'URLS', 'notificationService', 'loaderService', function($location, usersService, URLS, notificationService, loaderService){
    this.user = {
    }
    
    this.api = {
    }
    
    this.backToList = function(){
        $location.path(URLS.Pages.Users.List);
    }
    
    this.save = function(){
        if (this.api.isValid()){
            loaderService.show();
            usersService.save(this.user).success(function(){
                loaderService.hide();
                notificationService.notify("Užívateľ bol vytvorený");
                $location.path(URLS.Pages.Users.List);
            });
        }
    }
}]);

appModule.controller('EditUserController',['$routeParams', '$location', 'usersService', 'URLS', 'notificationService', 'loaderService', function($routeParams, $location, usersService, URLS, notificationService, loaderService){
    var self = this;
    
    this.user = {
    }
    
    this.api = {
    }

    this.currentTab = 'form';
    
    this.backToList = function(){
        $location.path(URLS.Pages.Users.List);
    }

    this.save = function(){
        if (this.api.isValid()){
            loaderService.show();
            usersService.save(this.user).success(function(){
                loaderService.hide();
                notificationService.notify("Užívateľ bol uložený");
                self.backToList();
            });
        }
    }
    
    this.remove = function(){
        usersService.remove($routeParams.userId).success(function(){
            notificationService.notify("Užívateľ bol zmazaný");
            self.backToList();
        });
    }
    
    var loadUser = function(){
        usersService.get($routeParams.userId).success(function(data){
            self.user = data;
        });
    }
    loadUser();
}]);