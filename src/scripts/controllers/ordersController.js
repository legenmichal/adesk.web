var appModule = angular.module('AppModule');

appModule.controller('OrdersController', ['$scope', 'ordersService', 'notificationService', 'loaderService', 'CALENDAR_CONFIG', 'uiCalendarConfig', '$sessionStorage', function($scope, ordersService, notificationService, loaderService, CALENDAR_CONFIG, uiCalendarConfig, $sessionStorage){
    var self = this;
    var cacheKey = 'ordersCalendar';
    self.orders = []; 
    self.datasource = [self.orders];
    self.datepicker = {};
    
    if (!loadState()){
       self.currentMomentDate = moment();
       loadOrders();
    }
    
    // Watch for moment date changes 
    $scope.$watch(
        function ( scope ) {
            return self.currentMomentDate;
        }, 
        function(newValue, oldValue){
            self.currentRawDate = self.currentMomentDate.toDate();
        }
    )
    
    // Watch for raw date changes
    $scope.$watch(
        function ( scope ) {
            return self.currentRawDate;
        }, 
        function(newValue, oldValue){
            if (self.currentRawDate != self.currentMomentDate.toDate()){
                self.currentMomentDate = moment(self.currentRawDate);
                
                var calendar = getCalendar();
                if (calendar){
                    calendar.fullCalendar('gotoDate', self.currentMomentDate);
                }
            }
        }
    )
    
	self.uiConfig = {
        calendar: {
            dayClick: function(date, jsEvent, view) {
                var order = {
                    TimeFrom: date.toDate()
                }
                self.api.show(order);
            },
            viewRender: function( view, calendar ){
                var calendar = getCalendar();
                if (calendar){
                    if (moment.isMoment(calendar.fullCalendar('getDate'))){
                        self.currentMomentDate = calendar.fullCalendar('getDate');
                        loadOrders();
                    }
                }
            },
            defaultDate: self.currentMomentDate,
            eventClick: function(event, jsEvent, view){
                self.api.show(event.order);
            },
            customButtons: {
                    showCalendarButton: {
                        text: 'Dátum',
                        click: function() {
                            self.datepicker.opened = true;
                            $scope.$apply();
                        }
                    }
                }
        }
    };
    angular.extend(self.uiConfig.calendar, CALENDAR_CONFIG);
    
    self.api = {
        save: function(order){
            loaderService.show();
            
            ordersService.save(order).success(function(){
                loaderService.hide();
                notificationService.notify("Objednávka bola uložená");
                
                // Load all orders
                loadOrders();
            });
        }
    }

    function getCalendar(){
        return uiCalendarConfig.calendars[cacheKey];
    }

    function loadOrders(clear){
        // Clear old orders
        self.orders.splice(0, self.orders.length); 
        
        ordersService.getListByDate(self.currentMomentDate.toISOString()).then(function(response){
            angular.forEach(response.data.Data, function(o){
                self.orders.push({
                    title: o.FirstName + ' ' + o.LastName,
                    start: o.TimeFrom,
                    end: o.TimeTo,
                    order: o
                });
            });
            
            // Save state
            saveState();
        })
    };

    function saveState(){
        var state = {
            currentMomentDate: self.currentMomentDate.toDate(),
            orders: self.orders 
        }

        $sessionStorage[cacheKey] = state;
    }
    
    function loadState(){
        // Restore state
        var state = $sessionStorage[cacheKey];
        
        if (angular.isDefined(state)){
            self.currentMomentDate = moment(state.currentMomentDate);
            angular.forEach(state.orders, function(o){
                self.orders.push(o);          
            });
            
            return true;
        }
        
        return false;
    }

}]);