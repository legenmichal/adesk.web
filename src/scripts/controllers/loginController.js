var loginModule = angular.module('LoginModule');

loginModule.controller('LoginController', ['$http', 'securityService', '$window', 'DEFAULT_PAGE', '$scope', function($http, securityService, $window, DEFAULT_PAGE, $scope) {
    var self = this;
    
    self.login = function() {
        var form = self.login_form;
        if (form.$valid) {
            self.showLoader = true;
            securityService.authenticate(self.username, self.password).then(function(data, status) {
                $window.location.href =  DEFAULT_PAGE;
            }, function(){
                form.submitted = true;
            }).finally(function(){
                self.showLoader = false;
            });
        } else {
          form.submitted = true;
        }
    }
}]);