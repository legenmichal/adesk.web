var constantsModule = angular.module('app-constants', []);

constantsModule.constant('ACCESS_LEVELS', {
    all: 0,
    user: 1,
    admin: 2
});

constantsModule.constant('USERS_PAGE', "/users");
constantsModule.constant('LOGIN_PAGE', "/login.html");
constantsModule.constant('DEFAULT_PAGE', "/");

constantsModule.constant('POPUP_TYPE', {
    error: 0,
    confirmation: 1,
    warning: 2
});

constantsModule.constant('URLS', 
    {
        Api: {
            Users: {
                List: 'http://localhost/users/getlist',
                Save: 'http://localhost/users/save',
                Get: 'http://localhost/users/get',
                Remove: 'http://localhost/users/remove',
                Search: 'http://localhost/users/search'
            },
            Orders: {
                GetList: 'http://localhost/orders/getlist',
                Save: 'http://localhost/orders/save',
                Remove: 'http://localhost/orders/remove'
            },
            OrderTypes: {
                List: 'http://localhost/ordertypes/getlist',
            }
        },
        Pages: {
            Default: '/',
            Users: {
                List: '/users',
                New: '/users/new',
                Edit: '/users/edit/{0}'
            },
            Login: '/login.html'
        }
    }
);

constantsModule.constant('CALENDAR_CONFIG', {
    height: 'auto',
    editable: true,
    header:{
        left: 'title',
        center: '',
        right: 'showCalendarButton today prev,next'
    },
    buttonText: {
      today: 'Dnes'  
    },
    defaultView: 'agendaDay',
    views:{
        agendaDay: {
            slotDuration: '00:15:00',
            slotLabelFormat : 'H:mm',
            minTime: '07:00:00',
            maxTime: '17:00:00',
            allDaySlot: false
        }
    }
});
